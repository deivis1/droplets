Digital Ocean droplet manager
==========================
***
List, create, remove droplets from shell
***
### Setup

- Set script permissions `chmod +x droplet.sh`

- Generate API token in your [Digital Ocean account](https://digitalocean.com) API section  
  You will be asked to enter this token during script execution

- You can edit default new droplet options in droplets.sh
***
```
###### Droplet parameters #######

DEFAULT_IMAGE=1
DEFAULT_SIZE=1
DEFAULT_REGION=2

#################################
```
***

You can find more information about API in [Digital Ocean documentation](https://developers.digitalocean.com/documentation/v2/)