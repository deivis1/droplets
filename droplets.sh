#/bin/bash

###### Droplet parameters #######

DEFAULT_IMAGE=1
DEFAULT_SIZE=1
DEFAULT_REGION=2

#################################

RED='\033[91m'
GREEN='\033[0;32m'
BLUE='\033[01;34m'
NC='\033[00m'      # No Color

SCRIPT=$(readlink -f $0 | cut -d'.' -f1)
SCRIPTNAME=`basename $SCRIPT`
SCRIPTPATH=`dirname $SCRIPT`
CONFIGFILE=$SCRIPTPATH/$SCRIPTNAME.conf

IMAGES=('ubuntu-18-04-x64' 'debian-9-x64' 'centos-7-x64' 'fedora-30-x64' 'freebsd-12-x64-zfs')

IMAGES1=('Ubuntu 18.04.3' 'Debian 9.7' 'CentOS 7.6' 'Fedora 30' 'FreeBSD 12.0')

REGIONS=()
REGIONS+=('ams2')
REGIONS+=('ams3')
REGIONS+=('blr1')
REGIONS+=('fra1')
REGIONS+=('lon1')
REGIONS+=('nyc1')
REGIONS+=('nyc2')
REGIONS+=('nyc3')
REGIONS+=('sfo1')
REGIONS+=('sfo2')
REGIONS+=('sgp1')
REGIONS+=('tor1')

REGIONS1=()
REGIONS1+=('Amsterdam 2')
REGIONS1+=('Amsterdam 3')
REGIONS1+=('Bangalore 1')
REGIONS1+=('France 1')
REGIONS1+=('London 1')
REGIONS1+=('New York 1')
REGIONS1+=('New York 2')
REGIONS1+=('New York 3')
REGIONS1+=('San Francisco 1')
REGIONS1+=('San Francisco 2')
REGIONS1+=('Singapure 1')
REGIONS1+=('Toronto1')

SIZES=()
SIZES+=('s-1vcpu-1gb')
SIZES+=('s-1vcpu-2gb')
SIZES+=('s-1vcpu-3gb')
SIZES+=('s-2vcpu-2gb')
SIZES+=('s-3vcpu-1gb')
SIZES+=('s-2vcpu-4gb')
SIZES+=('c-2')

SIZES1=()
SIZES1+=('1GB 1CPU 25 SSD - €5')
SIZES1+=('2GB 1CPU 50 SSD - €10')
SIZES1+=('3GB 1CPU 60 SSD - €15')
SIZES1+=('2GB 2CPU 60 SSD - €15')
SIZES1+=('1GB 3CPU 60 SSD - €15')
SIZES1+=('4GB 2CPU 80 SSD - €20')
SIZES1+=('8GB 4CPU 160 SSD - €40')

trap ctrl_c INT

function ctrl_c() {
  echo ""
  exit
}

function print_menu() {
  echo ""
  echo -e "${BLUE}Droplets${NC}"
  echo "1. List droplets"
  echo "2. Create droplet"
  echo "3. Remove droplet"
  echo "4. Assign to floating ip"
  echo "5. List ssh keys"
  echo "6. Create ssh key"
  echo "7. Remove ssh key"
  echo "8. Enter API token"
  echo "0. Exit"
}

function droplet_info () {
  item_no=$1
  ((item_no++))
  item_id=`echo $droplets | jq ".droplets[$1].id"`
  item_name=`echo $droplets | jq ".droplets[$1].name" | tr -d '"'`
  item_ip=`echo $droplets | jq ".droplets[$1].networks.v4[0].ip_address" | tr -d '"'`
  item_created=`echo $droplets | jq ".droplets[$1].created_at" | tr -d '"'`
  item_created=`date -d $item_created +'%Y-%m-%d %T'`
  item_status=`echo $droplets | jq ".droplets[$1].status" | tr -d '"'`
  item_image=`echo $droplets | jq ".droplets[$1].image.slug" | tr -d '"'`
  item_size=`echo $droplets | jq ".droplets[$1].size_slug" | tr -d '"'`
  item_region=`echo $droplets | jq ".droplets[$1].region.slug" | tr -d '"'`

  printf "${BLUE}%s${NC}  %-10s ${GREEN}%-15s${NC}  ${BLUE}%-11s${NC} ${GREEN}%-9s${NC} %-18s %-13s %-8s %s\n" \
    $item_no $item_id $item_ip $item_name $item_status "$item_image" "$item_size" "$item_region" "$item_created"
}

function list_droplets() {
  droplets=`get_droplets`
  n=`echo $droplets | jq '.meta.total'`
  ((n--))

  if [[ $n = -1 ]]; then
    echo -e "${RED}There are no running droplets${NC}"
    return
  fi

  echo "#  Id         IP Address       Name        Status    Image              Size          Region   Created"
  for((i=0;i<=n;++i)) do
    droplet_info $i
  done
}

function get_ssh_keys() {
  res=`curl -s -X GET "https://api.digitalocean.com/v2/account/keys" \
    -H "Authorization: Bearer $token" \
    -H "Content-Type: application/json"`

  key_count=`echo $res | jq '.meta.total'`

  for((i=0;i<key_count;++i)) do
    SSH_KEYS["id"$i]=`echo $res | jq ".ssh_keys[$i].id"`
    SSH_KEYS["name"$i]=`echo $res | jq ".ssh_keys[$i].name" | tr -d '"'`
    SSH_KEYS["fingerprint"$i]=`echo $res | jq ".ssh_keys[$i].fingerprint" | tr -d '"'`
  done
}

function print_ssh_keys() {
  if (( key_count == 0 )); then
    echo -e "${RED}No ssh keys found${NC}"
    return
  fi

  echo "#  Id         Name         Fingerprint"
  for((i=0;i<key_count;++i)) do
    item_no=$((i+1))
    item_id=${SSH_KEYS[id$i]}
    item_name=${SSH_KEYS[name$i]}
    item_fingerprint=${SSH_KEYS[fingerprint$i]}

    printf "${BLUE}%s${NC}  %-10s ${GREEN}%-11s${NC}  ${BLUE}%s${NC}\n" $item_no $item_id $item_name $item_fingerprint
  done
}

function create_droplet() {
  unset SSH_KEYS
  declare -A SSH_KEYS
  get_ssh_keys
  
  # Enter name

  read -p 'Enter new droplet name: ' name

  if [[ -z $name ]]; then
    echo -e "${RED}No action taken${NC}"
    return
  fi

  # Select image

  echo -e "\n${BLUE}Images${NC}"

  for((i=0;i<${#IMAGES1[*]};++i)) do
    echo -e "${BLUE}$((i+1))${NC} ${IMAGES1[$i]}"
  done

  valid=0
  while (( $valid == 0 )); do
    read -p "Choose image number [${IMAGES1[$((DEFAULT_IMAGE-1))]}]: " image
    valid=1

    if [[ -z $image ]]; then
      image=$DEFAULT_IMAGE
    fi

    if ! [[ $image =~ ^[0-9]?$ ]]; then
      echo -e "${RED}Only numbers accepted${NC}"
      valid=0
    fi

    if (( $valid == 1 )) && ((( $image < 1 )) || (( $image > ${#IMAGES1[@]} ))); then
      echo -e "${RED}Incorrect choice${NC}"
      valid=0
    fi
  done
  
  DROPLET_IMAGE=${IMAGES[$((image-1))]}

  # Select size

  echo -e "\n${BLUE}Sizes${NC}"

  for((i=0;i<${#SIZES1[*]};++i)) do
    echo -e "${BLUE}$((i+1))${NC} ${SIZES1[$i]}"
  done

  valid=0
  while (( $valid == 0 )); do
    read -p "Choose size number [${SIZES1[$((DEFAULT_SIZE-1))]}]: " size
    valid=1

    if [[ -z $size ]]; then
      size=$DEFAULT_SIZE
    fi

    if ! [[ $size =~ ^[0-9]?$ ]]; then
      echo -e "${RED}Only numbers accepted${NC}"
      valid=0
    fi

    if (( $valid == 1 )) && ((( $size < 1 )) || (( $size > ${#SIZES1[@]} ))); then
      echo -e "${RED}Incorrect choice${NC}"
      valid=0
    fi
  done
  
  DROPLET_SIZE=${SIZES[$((size-1))]}
  
  # Select region

  echo -e "\n${BLUE}Regions${NC}"

  for((i=0;i<${#REGIONS1[*]};++i)) do
    echo -e "${BLUE}$((i+1))${NC} ${REGIONS1[$i]}"
  done

  valid=0
  while (( $valid == 0 )); do
    read -p "Choose region number [${REGIONS1[$((DEFAULT_REGION-1))]}]: " region
    valid=1

    if [[ -z $region ]]; then
      region=$DEFAULT_REGION
    fi

    if ! [[ $region =~ ^[0-9]?$ ]]; then
      echo -e "${RED}Only numbers accepted${NC}"
      valid=0
    fi

    if (( $valid == 1 )) && ((( $region < 1 )) || (( $region > ${#REGIONS1[@]} ))); then
      echo -e "${RED}Incorrect choice${NC}"
      valid=0
    fi

  done
  
  DROPLET_REGION=${REGIONS[$((region-1))]}

  # Select ssh key

  if (( $key_count == 0 )); then
    echo -e "${RED}Please create at least one ssh key${NC}"
    echo -e "${RED}Droplet was not created${NC}"
    return
  fi

  if (( $key_count == 1 )); then
    key_no=1
  else
    print_ssh_keys

    valid=0
    while (( $valid == 0 )); do
      read -p 'Choose ssh key number: ' key_no
      valid=1

      if [[ -z $key_no ]]; then
        echo -e "${RED}Please enter number${NC}"
        valid=0
      fi

      if ! [[ $key_no =~ ^[0-9]?$ ]]; then
        echo -e "${RED}Only numbers accepted${NC}"
        valid=0
      fi

      if (( $valid == 1 )) && ((( $key_no < 1 )) || (( $key_no > $key_count ))); then
        echo -e "${RED}Incorrect choice${NC}"
        valid=0
      fi
    done
  fi

  ((key_no--))

  DROPLET_SSH_KEY=${SSH_KEYS["id"$key_no]}

  echo -e "\n${BLUE}Selected settings${NC}"
  echo -e "Name:    $name"
  echo -e "Image:   ${IMAGES[$((image-1))]} / ${GREEN}${IMAGES1[$((image-1))]}${NC}"
  echo -e "Size:    ${SIZES[$((size-1))]} / ${GREEN}${SIZES1[$((size-1))]}${NC}"
  echo -e "Region:  ${REGIONS[$((region-1))]} / ${GREEN}${REGIONS1[$((region-1))]}${NC}"
  echo -e "SSH key: ${SSH_KEYS[name$key_no]} / ${GREEN}${SSH_KEYS[fingerprint$key_no]}${NC}"

  echo -ne "\nDo you want to continue [y/n]? "
  read confirm
  confirm=`echo $confirm | tr "[:upper:]" "[:lower:]"`

  if [[ "$confirm" != "y" ]] && [[ "$confirm" != "yes" ]]; then
    echo -e "${RED}User cancelled action${NC}"
    return
  fi  

  resp=`curl -s -X POST "https://api.digitalocean.com/v2/droplets" \
    -d '{"name":"'$name'","region":"'$DROPLET_REGION'","size":"'$DROPLET_SIZE'","image":"'$DROPLET_IMAGE'","ssh_keys": ['$DROPLET_SSH_KEY'],"backups": false,"ipv6": false,"monitoring": false,"user_data": null,"private_networking": null,"volumes": null}' \
    -H "Authorization: Bearer $token" \
    -H "Content-Type: application/json"`

  check=`echo $resp | jq ".droplet"`

  if [[ $check = "null" ]]; then
    message=`echo $resp | jq ".message" | tr -d '"'`
    echo -e "\n${RED}Droplet was not created\nError: $message${NC}"
  else
    echo -e "\n${GREEN}Creating droplet\n${NC}List droplets in 5-10 seconds"
  fi
}

function assign_floating_ip() {
  ips=`curl -s -X GET "https://api.digitalocean.com/v2/floating_ips?page=1&per_page=20" \
    -H "Authorization: Bearer $token" \
    -H "Content-Type: application/json"`

  ip_count=`echo $ips | jq '.meta.total'`

  if (( ip_count == 0 )); then
    echo -e "\n${RED}There are no floating ips${NC}"
    return
  fi

  droplets=`get_droplets`
  droplet_count=`echo $droplets | jq '.meta.total'`

  if (( $droplet_count == 0 )); then
    echo -e "${RED}There are no running droplets${NC}"
    return
  fi

  for((i=0;i<droplet_count;++i)) do
    ids[$i]=`echo $droplets | jq ".droplets[$i].id"`
  done
  
  if (( $droplet_count == 1 )); then
    no=1
    valid=1
  else
    valid=0
  fi

  ip=`echo $ips | jq '.floating_ips[0].ip' | tr -d '"'`

  while (( valid == 0 )); do
    echo -ne "Enter droplet to assign to floating ip ${GREEN}$ip${NC} [1-$droplet_count]: "
    read no
    valid=1

    if [[ -z $no ]]; then
      echo -e "${RED}No action taken${NC}"
      return
    fi

    if ! [[ $no =~ ^[0-9]?$ ]]; then
      echo -e "${RED}Only numbers accepted${NC}"
      valid=0
    fi

    if (( valid == 1 )) && ((( $no < 1 )) || (( $no > $droplet_count ))); then
      echo -e "${RED}Incorrect choice${NC}"
      return
    fi

  done

  id=${ids[$((no-1))]}

  resp=`curl -s -X POST "https://api.digitalocean.com/v2/floating_ips/$ip/actions" \
    -d '{"type":"assign","droplet_id":'$id'}' \
    -H "Authorization: Bearer $token" \
    -H "Content-Type: application/json"`

  check=`echo $resp | jq ".action"`

  if [[ $check = "null" ]]; then
    message=`echo $resp | jq ".message" | tr -d '"'`
    echo -e "${RED}Droplet was not assigned\nError: $message${NC}"
  else
    echo -e "${GREEN}Droplet assigned${NC}"
  fi
}

function remove_droplet() {
  droplets=`get_droplets`
  droplet_count=`echo $droplets | jq '.meta.total'`

  if (( $droplet_count == 0 )); then
    echo -e "${RED}There are no running droplets${NC}"
    return
  fi

  for((i=0;i<droplet_count;++i)) do
    ids[$i]=`echo $droplets | jq ".droplets[$i].id"`
  done
  
  if (( $droplet_count == 1 )); then
    no=1
    valid=1
  else
    valid=0
  fi
  
  while (( valid == 0 )); do
    echo -ne "Enter droplet number to delete [1-$droplet_count]: "
    read no
    valid=1

    if [[ -z $no ]]; then
      echo -e "${RED}No action taken${NC}"
      return
    fi

    if ! [[ $no =~ ^[0-9]?$ ]]; then
      echo -e "${RED}Only numbers accepted${NC}"
      valid=0
    fi

    if (( valid == 1 )) && ((( $no < 1 )) || (( $no > $droplet_count ))); then
      echo -e "${RED}Incorrect choice${NC}"
      return
    fi

  done

  curl -s -X DELETE "https://api.digitalocean.com/v2/droplets/${ids[$((no-1))]}" \
    -H "Authorization: Bearer $token" \
    -H "Content-Type: application/json" \
    --output /dev/null

  echo -e "${RED}Removing droplet\n${NC}List droplets in 5-10 seconds"
}

function enter_api_token() {
  token=""
  while [ -z $token ]; do
    read -p 'Enter token: ' token

    if [[ -z $token ]] && [[ $1 != "1" ]]; then
      echo -e "${RED}No action taken${NC}"
      return
    fi
  done
  echo $token > $CONFIGFILE
  echo -e "${GREEN}Token saved in $CONFIGFILE${NC}\n${RED}This file has to be kept secure${NC}"
}

function get_droplets() {
  curl -s -X GET "https://api.digitalocean.com/v2/droplets" \
    -H "Authorization: Bearer $token" \
    -H "Content-Type: application/json"
}

function list_ssh_keys() {
  unset SSH_KEYS
  declare -A SSH_KEYS
  get_ssh_keys
  print_ssh_keys
}

function create_ssh_key() {
  read -p 'Enter new ssh key name: ' ssh_name

  if [[ -z $ssh_name ]]; then
    echo -e "${RED}No action taken${NC}"
    return
  fi

  read -p 'Enter ssh key body: ' ssh_body

  ssh_body=`echo $ssh_body | sed 's/ *$//g'`

  if [[ -z $ssh_body ]]; then
    echo -e "${RED}No action taken${NC}"
    return
  fi

  resp=`curl -s -X POST "https://api.digitalocean.com/v2/account/keys" \
    -d '{"name":"'$ssh_name'","public_key":"'"$ssh_body"'"}' \
    -H "Authorization: Bearer $token" \
    -H "Content-Type: application/json"`

  check=`echo $resp | jq ".ssh_key"`

  if [[ $check = "null" ]]; then
    message=`echo $resp | jq ".message" | tr -d '"'`
    echo -e "\n${RED}SSH key was not created\nError: $message${NC}"
  else
    echo -e "\n${GREEN}SSH key has been created${NC}"
  fi
}

function remove_ssh_key() {
  unset SSH_KEYS
  declare -A SSH_KEYS
  get_ssh_keys
  print_ssh_keys

  if (( $key_count == 0 )); then
    echo -e "${RED}There are no ssh keys${NC}"
    return
  fi

  if (( $key_count == 1 )); then
    key_no=1
    valid=1
  else
    valid=0
  fi
  
  while (( valid == 0 )); do
    echo -ne "Enter ssh key number to delete [1-$key_count]: "
    read key_no
    valid=1

    if [[ -z $key_no ]]; then
      echo -e "${RED}No action taken${NC}"
      return
    fi

    if ! [[ $key_no =~ ^[0-9]?$ ]]; then
      echo -e "${RED}Only numbers accepted${NC}"
      valid=0
    fi

    if (( valid == 1 )) && ((( key_no < 1 )) || (( key_no > key_count ))); then
      echo -e "${RED}Incorrect choice${NC}"
      return
    fi

  done
 
  curl -s -X DELETE "https://api.digitalocean.com/v2/account/keys/${SSH_KEYS[id$((key_no-1))]}" \
    -H "Authorization: Bearer $token" \
    -H "Content-Type: application/json" \
    --output /dev/null

  echo -e "${RED}SSH key has been removed${NC}"
}

droplets=`get_droplets`

if [ ! -f $CONFIGFILE ]
then
  enter_api_token 1
else
  token=$(<$CONFIGFILE)
fi

option=""

while [[ $option != "0" ]]
do
  print_menu
  
  option=" "
  while ! [[ $option =~ ^[0-9]?$ ]]; do
    echo -ne "${BLUE}Select option: ${NC}"
    read option

    if ! [[ $option =~ ^[0-9]?$ ]]; then
      echo -e "${RED}Only numbers accepted${NC}"
    fi
  done

  case $option in
    "1") list_droplets;;
    "2") create_droplet;;
    "3") remove_droplet;;
    "4") assign_floating_ip;;
    "5") list_ssh_keys;;
    "6") create_ssh_key;;
    "7") remove_ssh_key;;
    "8") enter_api_token;;
    "0") exit;;
    "") exit;;
    *) if [[ $option != "0" ]]; then
      echo -e "${RED}Incorrect option.${NC}\nPlease enter numbers 1-4 or 0"
    fi
    ;;
  esac

  option=""
done